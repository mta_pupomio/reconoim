# frozen_string_literal: true

Rails.application.routes.draw do
  root 'home#index'
  devise_for :users

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  #
  resource :products, only: [:show, :create, :destroy]
  resource :customers

  ProductAction.descendants.each do |pa|
    resource pa.name.downcase+"s"
  end

  get '/history',to: 'history#index', as: 'history'
  get '/search',to: 'search#product_search', as: 'search'

end
