class SoldsController < ProductActionsController
  # POST /product_actions
  # POST /product_actions.json
  def create
    new_params = params.require(:sold).permit(:comment, :product_id, :sold_price)
    @product_action = Sold.new(new_params)

    respond_to do |format|
      if @product_action.save
        format.html { redirect_to root_path, notice: 'Product action was successfully created.' }
        format.json { render :show, status: :created, location: @product_action }
      else
        format.html {
          redirect_to root_path,
                      alert: 'Product action was not successfully created.' + @product_action.errors.inspect.to_s }
        puts @product_action.errors.inspect
        format.json { render json: @product_action.errors, status: :unprocessable_entity }
      end
    end
  end
end