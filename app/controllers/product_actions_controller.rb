

class ProductActionsController < ApplicationController
  before_action :set_product_action, only: [:show, :edit, :update, :destroy]

  # GET /product_actions
  # GET /product_actions.json
  def index
    @product_actions = ProductAction.all
  end

  # GET /product_actions/1
  # GET /product_actions/1.json
  def show
  end

  # GET /product_actions/new
  def new
    @product_action = ProductAction.new
  end

  # GET /product_actions/1/edit
  def edit
  end

  # POST /product_actions
  # POST /product_actions.json
  def create
    @product_action = ProductAction.new(product_action_params)

    respond_to do |format|
      if @product_action.save
        format.html { redirect_to @product_action, notice: 'Product action was successfully created.' }
        format.json { render :show, status: :created, location: @product_action }
      else
        format.html { render :new }
        format.json { render json: @product_action.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /product_actions/1
  # PATCH/PUT /product_actions/1.json
  def update
    respond_to do |format|
      if @product_action.update(product_action_params)
        format.html { redirect_to @product_action, notice: 'Product action was successfully updated.' }
        format.json { render :show, status: :ok, location: @product_action }
      else
        format.html { render :edit }
        format.json { render json: @product_action.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /product_actions/1
  # DELETE /product_actions/1.json
  def destroy
    @product_action.destroy
    respond_to do |format|
      format.html { redirect_to product_actions_url, notice: 'Product action was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product_action
      @product_action = ProductAction.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_action_params
      params.fetch(:product_action, {})
    end
end

class CreatedsController < ProductActionsController

end
class RecyclesController < ProductActionsController

end



