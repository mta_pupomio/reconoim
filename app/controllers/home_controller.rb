class HomeController < ApplicationController
  def index
    @product = Product.new
    @customer = Customer.new
    @products = Product.where(active: true)
    @actions = ProductAction.descendants
  end
end
