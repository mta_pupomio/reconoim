class HistoryController < ApplicationController
  def index
    @product = Product.new
    @customer = Customer.new
    @products = Product.where(active: false)
  end
end
