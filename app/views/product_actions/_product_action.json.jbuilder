json.extract! product_action, :id, :created_at, :updated_at
json.url product_action_url(product_action, format: :json)
