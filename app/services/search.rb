require 'net/http'
require 'base64'
class Search
  def initialize

    @api_key = ENV['AppId']
    @dev_id = ENV['DevId']
    @cert_id = ENV['CertId']
    @auth_token = ENV['AuthToken']
  end

  def search(term)

    url = URI.parse('https://api.sandbox.ebay.com/ws/api.dll/identity/v1/oauth2/token')
    encoded_creds = Base64.strict_encode64("#{@api_key}:#{@cert_id}")
    req = Net::HTTP::Post.new(url.to_s,{
        Authorization: "Basic #{encoded_creds}"
    })
    
    req.body = {

    }.to_json
    p req

    http = Net::HTTP.new(url.host, url.port)
    http.set_debug_output($stdout)
    http.use_ssl = true
    http.request(req) do |resp|
      p resp.code
    end
    http.request(req) do |resp|
      p resp.body
    end

  end
end


