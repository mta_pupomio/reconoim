# frozen_string_literal: true

class ProductAction < ApplicationRecord
  belongs_to :product
  after_create :mark_project_inactive

  def form_fields
    [:comment]
  end

  def action_name
    "GenericAction"
  end

  def get_tooltip
    tooltip = format("On %s\n", date.to_s)
    puts attributes
    form_fields.each do |field|
      tooltip = format("%s%s: %s\n", tooltip, field.to_s.humanize, attributes[field.to_s].to_s)
    end
    puts tooltip
    tooltip
  end

  def self.automatic?
    false
  end

  def descendants
    ObjectSpace.each_object(Class).select { |klass| klass < self }
  end

  def terminator?
    false
  end

  def mark_project_inactive
    if terminator?
      product.active = false
      product.save
    end
  end
end
class Received < ProductAction
  def self.automatic?
    true
  end
end
class Recycle < ProductAction
  def terminator?
    true
  end
end
class Dismantle < ProductAction
  def form_fields
    [:salvageable_parts] + super
  end
  def terminator?
    true
  end
end
class Donate < ProductAction
  def terminator?
    true
  end
end
class Repair < ProductAction
  def form_fields
    %i[cost hours] + super
  end
end
class Test < ProductAction
  def form_fields
    %i[cost hours result test_purpose] + super
  end
end
class Listed < ProductAction
  def form_fields
    %i[listed_price ebay_id] + super
  end
end

class Sold < ProductAction
  def form_fields
    %i[sold_price] + super
  end
  def terminator?
    true
  end
end

class Misc < ProductAction
end
