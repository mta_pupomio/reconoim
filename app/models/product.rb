# frozen_string_literal: true
require 'product_action'
class Product < ApplicationRecord
  has_many :product_actions, dependent: :destroy
  before_destroy :remove_actions
  after_create :add_creation_action

  def remove_actions
    product_actions.each(&:delete)
  end

  private

  def add_creation_action
    action = Received.new product_id: id
    action.save
  end
end
