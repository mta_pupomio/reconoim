// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require activestorage
//= require turbolinks
//= require jquery3
//= require jquery_ujs
//= require dataTables/jquery.dataTables
//= require bootstrap

//= require_tree .

$(document).on('turbolinks:load', function()
{
    $(".alert").fadeTo(2000, 500).slideUp(500, function () {
        $(".alert").slideUp(500);
    });

    $('.dataTable').DataTable( {
        "initComplete": function () {
            this.api().columns([1,2,3,4]).every( function () {
                var column = this;

                var select = $('<select><option value=""></option></select>')
                    .appendTo( $(column.header()) )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );

                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }
    } );
} );
function addAction(product, actions) {
    console.log(product.name);
     $('.modal-title', $("#add_action")).html("ID:"+product.id+"<br>Name: "+product.name);
    $(".product_id").val(product.id);
    $("#add_action").modal("show");
}

function show_select_tab(el) {
    $(el.selectedOptions[0]).tab('show');

}