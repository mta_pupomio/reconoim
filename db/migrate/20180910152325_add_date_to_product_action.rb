class AddDateToProductAction < ActiveRecord::Migration[5.2]
  def change
    add_column :product_actions, :date, :datetime, default: DateTime.now
    add_column :product_actions, :comment, :text
  end
end
