class AddTypeToProductAction < ActiveRecord::Migration[5.2]
  def change
    add_column :product_actions, :type, :string
  end
end
