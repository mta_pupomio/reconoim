class AlterProductAction < ActiveRecord::Migration[5.2]
  def change
    change_column :product_actions, :date, :datetime, default: -> {'CURRENT_TIMESTAMP'}
  end
end
