class AddDataToTest < ActiveRecord::Migration[5.2]
  def change
    add_column :product_actions, :test_purpose, :text
    add_column :product_actions, :result, :text
  end
end
