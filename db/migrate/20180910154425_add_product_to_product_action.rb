class AddProductToProductAction < ActiveRecord::Migration[5.2]
  def change
    add_reference :product_actions, :product, foreign_key: true, on_delete: :cascade
  end
end
