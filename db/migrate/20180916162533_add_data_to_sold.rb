class AddDataToSold < ActiveRecord::Migration[5.2]
  def change
    add_column :product_actions, :sold_price, :float
  end
end
