class AddFieldsToProduct < ActiveRecord::Migration[5.2]
  def change
    add_column :products, :name, :text
    add_column :products, :serial_number, :text
    add_column :products, :model_number, :text
    add_column :products, :condition, :text
  end
end
