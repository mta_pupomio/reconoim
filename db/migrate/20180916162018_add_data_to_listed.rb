class AddDataToListed < ActiveRecord::Migration[5.2]
  def change
    add_column :product_actions, :listed_price, :float
    add_column :product_actions, :ebay_id, :text
  end
end
