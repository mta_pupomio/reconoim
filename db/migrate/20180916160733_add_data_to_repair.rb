class AddDataToRepair < ActiveRecord::Migration[5.2]
  def change
    add_column :product_actions, :cost, :float
    add_column :product_actions, :hours, :float
  end
end
