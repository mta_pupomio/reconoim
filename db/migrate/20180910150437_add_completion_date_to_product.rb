class AddCompletionDateToProduct < ActiveRecord::Migration[5.2]
  def change
    add_column :products, :completed_on, :datetime
  end
end
