class AddActiveToProduct < ActiveRecord::Migration[5.2]
  def change
    add_column :products, :active, :bool, default: true
  end
end
