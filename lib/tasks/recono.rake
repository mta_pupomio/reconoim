# frozen_string_literal: true

namespace :recono do
  desc 'Remove all products - testing only'
  task delete_all_products: :environment do
    Product.all.each do |p|
      p.remove_actions
      p.delete
    end
  end
  desc 'Create Admin User'
  task create_admin_user: :environment do
    @user = User.new(:email => ENV['ADMIN_USER'], :password => ENV['ADMIN_PASS'], :password_confirmation => ENV['ADMIN_PASS'])
    @user.is_admin = true
    @user.save

  end
end
