require 'test_helper'

class ProductActionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @product_action = product_actions(:one)
  end

  test "should get index" do
    get product_actions_url
    assert_response :success
  end

  test "should get new" do
    get new_product_action_url
    assert_response :success
  end

  test "should create product_action" do
    assert_difference('ProductAction.count') do
      post product_actions_url, params: { product_action: {  } }
    end

    assert_redirected_to product_action_url(ProductAction.last)
  end

  test "should show product_action" do
    get product_action_url(@product_action)
    assert_response :success
  end

  test "should get edit" do
    get edit_product_action_url(@product_action)
    assert_response :success
  end

  test "should update product_action" do
    patch product_action_url(@product_action), params: { product_action: {  } }
    assert_redirected_to product_action_url(@product_action)
  end

  test "should destroy product_action" do
    assert_difference('ProductAction.count', -1) do
      delete product_action_url(@product_action)
    end

    assert_redirected_to product_actions_url
  end
end
