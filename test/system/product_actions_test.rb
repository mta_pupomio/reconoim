require "application_system_test_case"

class ProductActionsTest < ApplicationSystemTestCase
  setup do
    @product_action = product_actions(:one)
  end

  test "visiting the index" do
    visit product_actions_url
    assert_selector "h1", text: "Product Actions"
  end

  test "creating a Product action" do
    visit product_actions_url
    click_on "New Product Action"

    click_on "Create Product action"

    assert_text "Product action was successfully created"
    click_on "Back"
  end

  test "updating a Product action" do
    visit product_actions_url
    click_on "Edit", match: :first

    click_on "Update Product action"

    assert_text "Product action was successfully updated"
    click_on "Back"
  end

  test "destroying a Product action" do
    visit product_actions_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Product action was successfully destroyed"
  end
end
